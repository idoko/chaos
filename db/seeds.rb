def seed_channels
    default = ["Meta", "Art", "Technology", "Writing", "Events"]

    default.each do |channel|
        Channel.create(name: channel, description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).")
    end
end