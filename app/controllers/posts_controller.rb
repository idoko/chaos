class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def show
    @post = Post.find(params[:id])
    @comment = Comment.new
    @replies = Comment.all
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = 1
    @post.channel_id = 1
    if @post.save
      redirect_to @post
    else
      redirect_to new_post_path
    end
  end

  private
  def post_params
    params.require(:post).permit(:title, :content)
  end
end
