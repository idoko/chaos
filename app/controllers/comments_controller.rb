class CommentsController < ApplicationController

  def create
    @comment = Comment.new(comment_params)
    @comment.parent_id = 0
    @comment.save
    post = Post.find(@comment.post)
    redirect_to post
  end

  private
  def comment_params
    params.require(:comment).permit(:comment, :user_id, :post)
  end
end